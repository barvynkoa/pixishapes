/**
 * Created by Andrii on 23.03.2017.
 */

var fl = fl || {};
fl.SpawnView = function (model, stage, renderer) {
    this._model = model;
    this.stage = stage;
    this.renderer = renderer;
    this.viewVidth = 640;
    this.viewHeight = 480;

    var bgGraphics = new PIXI.Graphics();
    bgGraphics.beginFill(0x061639);
    bgGraphics.drawRect(0, 0, this.viewVidth, this.viewHeight);
    bgGraphics.endFill();
    bgGraphics.boundsPadding = 0;
    var texture = bgGraphics.generateTexture();
    var bgSprite = new PIXI.Sprite(texture);
    this.interactive = true;
    this.stage.addChild(bgSprite);

    this.plusGravityButton = document.getElementById('plus_gravity');
    this.minusGravityButton = document.getElementById('minus_gravity');
    this.plusShapesButton = document.getElementById('plus_shapes');
    this.minusShapesButton = document.getElementById('minus_shapes');

    this.gravityValueIndicator = document.getElementById('gravity_value');
    this.shapesValueIndicator = document.getElementById('shapes_value');

    this.currentShapesIndicator = document.getElementById('current_shapes');
    this.surfaceAreaIndicator = document.getElementById('surface_area');
};

fl.SpawnView.prototype = {
    update: function () {
        var gravity = this._model.getGravity();
        var density = this._model.getDensity();
        var items = this._model.getItems();
        var surface = this._model.getSurfaceArea();
        items.forEach(function (item) {
            item.x += gravity.x;
            item.y += gravity.y;
        }, this);

        if (gravity.y != parseInt(this.gravityValueIndicator.innerHTML)) {
            this.gravityValueIndicator.innerHTML = "" + gravity.y;
        }

        if (density != parseInt(this.shapesValueIndicator.innerHTML)) {
            this.shapesValueIndicator.innerHTML = "" + density;
        }

        if (items.length != parseInt(this.currentShapesIndicator.innerHTML)) {
            this.currentShapesIndicator.innerHTML = "" + items.length;
        }

        if (surface != parseInt(this.surfaceAreaIndicator.innerHTML)) {
            this.surfaceAreaIndicator.innerHTML = "" + surface;
        }


    },

    createItem: function (x, y) {
        var randomArr = [
            fl.GenericShape.RECT,
            fl.GenericShape.CIRCLE,
            fl.GenericShape.ELLIPSE,
            fl.GenericShape.TRIANGLE,
            fl.GenericShape.PENTAGON,
            fl.GenericShape.HEXAGON,
            fl.GenericShape.RANDOM
        ];

        randomArr.sort(function () {
            return .5 - Math.random();
        });

        return new fl.GenericShape(this.stage, x, y, randomArr[0]);
    }
};

fl.SpawnView.constructor = fl.SpawnView;