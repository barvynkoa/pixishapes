/**
 * Created by Andrii on 23.03.2017.
 */
var fl = fl || {};
fl.GenericShape = function (stage, x, y, type) {
    this.type = type || fl.GenericShape.RANDOM;
    this.color = Math.floor(Math.random() * 0xffffff);
    this.randomize = 200;
    this.stage = stage;

    var graphics = this._drawGraphics();
    graphics.boundsPadding = 0;
    var texture = graphics.generateTexture();
    PIXI.Sprite.call(this, texture);
    this.interactive = true;
    this.anchor.set(0.5);

    this.x = x || 0;
    this.y = y || 0;

    this.scale.set(Math.random()*.5+1);
    this.rotation = Math.random()*Math.PI*2;

    this.click = function (e) {
        console.log('Primitive clicked: ' + this.type);
        if (stage) {
            stage.removeChild(this);
        }
    };

    if (stage) {
        stage.addChild(this);
    }
};

fl.GenericShape.prototype = Object.create(PIXI.Sprite.prototype);
fl.GenericShape.constructor = fl.GenericShape;

fl.GenericShape.RANDOM = 0;
fl.GenericShape.TRIANGLE = 1;
fl.GenericShape.PENTAGON = 2;
fl.GenericShape.HEXAGON = 3;
fl.GenericShape.RECT = 4;
fl.GenericShape.CIRCLE = 5;
fl.GenericShape.ELLIPSE = 6;

fl.GenericShape.prototype._drawGraphics = function () {
    var graphics = new PIXI.Graphics();

    graphics.beginFill(this.color);

    switch (this.type) {
        case fl.GenericShape.TRIANGLE:
            this._drawRandomTriangle(graphics);
            break;
        case fl.GenericShape.PENTAGON:
            this._drawRandomPentagon(graphics);
            break;
        case fl.GenericShape.HEXAGON:
            this._drawRandomHexagon(graphics);
            break;
        case fl.GenericShape.RECT:
            this._drawRandomRect(graphics);
            break;
        case fl.GenericShape.CIRCLE:
            this._drawRandomCircle(graphics);
            break;
        case fl.GenericShape.ELLIPSE:
            this._drawRandomEllipse(graphics);
            break;
        case fl.GenericShape.RANDOM:
            this._drawRandomCloud(graphics);
            break;
    }

    graphics.endFill();

    return graphics;
};

fl.GenericShape.prototype._drawRandomTriangle = function (graphics) {
    
    var self = this;
    var vertices = [-25, 50, 25, 50, 0, 0];
    vertices.forEach(function (part, index, theArray) {
        theArray[index] += Math.floor(Math.random()*self.randomize/20 - self.randomize/20);
    });
    graphics.drawPolygon(vertices);
};

fl.GenericShape.prototype._drawRandomRect = function (graphics) {
    var randomWidth = Math.floor(Math.random()*this.randomize/4 + this.randomize/4),
        randomHeight = Math.floor(Math.random()*this.randomize/4 + this.randomize/4);
    graphics.drawRect(0, 0, randomWidth, randomHeight);
};

fl.GenericShape.prototype._drawRandomCircle = function (graphics) {
    var randomRadius = Math.floor(Math.random()*this.randomize/10 + this.randomize/10);
    graphics.drawCircle(0, 0, randomRadius);
};

fl.GenericShape.prototype._drawRandomEllipse = function (graphics) {
    var randomWidth = Math.floor(Math.random()*this.randomize/10 + this.randomize/10);
    var randomHeight = Math.floor(Math.random()*this.randomize/10 + this.randomize/10);
    graphics.drawEllipse(0, 0, randomWidth, randomHeight);
};

fl.GenericShape.prototype._drawRandomPentagon = function (graphics) {
   var self = this;
    var vertices = [62, 0, 125, 44, 100, 119, 21, 119, 0, 46];
    vertices.forEach(function (part, index, theArray) {
        theArray[index] += Math.floor(Math.random()*self.randomize/20 - self.randomize/20);
    });
    graphics.drawPolygon(vertices);
};

fl.GenericShape.prototype._drawRandomHexagon = function (graphics) {
    var self = this;
    var vertices = [15, 0, 50, 0, 67, 27, 50, 57, 18, 59, 0, 30];
    vertices.forEach(function (part, index, theArray) {
        theArray[index] += Math.floor(Math.random()*self.randomize/20 - self.randomize/20);
    });
    graphics.drawPolygon(vertices);
};

fl.GenericShape.prototype._drawRandomCloud = function (graphics) {
    graphics.moveTo(22, 11);
    graphics.arcTo(41, -8, 58, 11, 18);
    graphics.arcTo(86, 20, 65, 48, 18);
    graphics.arcTo(62, 74, 32, 61, 18);
    graphics.arcTo(-4, 69, 4, 40, 18);
    graphics.arcTo(-2, 11, 22, 11, 18);
};

fl.GenericShape.prototype.destroyShape = function () {
    if (this.stage) {
        this.stage.removeChild(this);
    }
    this.destroy();
};