/**
 * Created by Andrii on 23.03.2017.
 */

var fl = fl || {};

fl.SpawnModel = function () {

    /**
     * Speed of falling items
     * @type {{x: number, y: number}}
     * @private
     */
    this._gravity = {x: 0, y: 1};

    /**
     * Density of shapes per second
     * @type {number}
     * @private
     */
    this._density = 3;

    /**
     * Gravity and density per step
     * @type {number}
     * @private
     */
    this._step = 1;

    /**
     * All items
     * @type {Array}
     * @private
     */
    this._items = [];

    /**
     * Height of spawn component
     * @type {number}
     * @private
     */
    this._pawnHeight = 600;

    /**
     * The surface area of all items
     * @type {number}
     * @private
     */
    this._surfaceArea = 0;
};

fl.SpawnModel.prototype = {
    addItem: function (item) {
        this._items.push(item);
        this.updateSurfaceArea(Math.floor(item.width*item.height));
    },

    removeItemAt: function (index) {
        var item = this._items[index];
        this._items.splice(index, 1);
        this.updateSurfaceArea(Math.floor(-item.width*item.height));
    },

    removeItem: function (item) {
        var index = this._items.indexOf(item);
        this._items.splice(index, 1);
        this.updateSurfaceArea(Math.floor(-item.width*item.height));
    },

    getItems: function () {
        return this._items;
    },

    getGravity: function () {
        return this._gravity;
    },

    setGravity: function (value) {
        this._gravity = value;
    },

    increaseDensity: function () {
        this._density+=this._step;
    },

    decreaseDensity: function () {
        if (this._density == 0) {
            return;
        }
        this._density-=this._step;
    },

    increaseGravityY: function () {
        this._gravity.y+=this._step;
    },

    decreaseGravityY: function () {
        if (this._gravity.y == 0) {
            return;
        }
        this._gravity.y-=this._step;
    },

    increaseGravityX: function () {
        this._gravity.x+=this._step;
    },

    decreaseGravityX: function () {
        if (this._gravity.x = 0) {
            return;
        }
        this._gravity.x-=this._step;
    },

    getDensity: function () {
        return this._density;
    },

    getSpawnHeight: function () {
        return this._pawnHeight;
    },


    getSurfaceArea: function () {
        return this._surfaceArea;
    },

    updateSurfaceArea: function (value) {
        this._surfaceArea += value;
    }
};

fl.SpawnModel.constructor = fl.SpawnModel;