/**
 * Created by Andrii on 23.03.2017.
 */

var fl = fl || {};
fl.SpawnController = function (model, view) {
    this._model = model;
    this._view = view;

    this._frameRate = 60;
    this._ticker = 1/this._frameRate;

     this._view.stage.interactive = true;
     this._view.stage.on('pointertap', function (event) {

         // get all pixels to calculate surface... Low performance.
         /*var pixels = this._view.renderer.extract.pixels(this._view.stage);
         var bgPixelsLength = pixels.filter(function(value, index) {
             return ((index % 3 == 0) && value == 6) ||
                 (((index - 1) % 3 == 0) && value == 22)  ||
                 (((index - 2) % 3 == 0) && value == 57)
         }).length;
         console.log(pixels.length - bgPixelsLength);*/

         if (!event.target.type) {
             var position = event.data.getLocalPosition(event.target);
             var item = this._view.createItem(position.x, position.y);
             this._model.addItem(item);
         }
     }, this);

    var self = this;
    this._view.plusGravityButton.addEventListener("click", function(){
        self._model.increaseGravityY();
    });

    this._view.minusGravityButton.addEventListener("click", function(){
        self._model.decreaseGravityY();
    });

    this._view.plusShapesButton.addEventListener("click", function(){
        self._model.increaseDensity();
    });

    this._view.minusShapesButton.addEventListener("click", function(){
        self._model.decreaseDensity();
    });
};

fl.SpawnController.prototype = {
    update: function () {
        if (Math.floor(this._ticker++) == this._frameRate) {

            for (var i = 0; i < this._model.getDensity(); i++) {
                var x = Math.floor(Math.random()*640);
                var y = Math.floor(-Math.random()*100 - 100);
                var item = this._view.createItem(x, y);
                this._model.addItem(item);
            }

            this._ticker = 0;
        }

        var items = this._model.getItems();
        items.forEach(function (item) {
            if (item.y > this._model.getSpawnHeight()) {
                this._model.removeItem(item);
                item.destroyShape();
            }
        }, this);
    }
};

fl.SpawnController.constructor = fl.SpawnController;