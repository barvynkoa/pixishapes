/**
 * Initial application logic
 * fl - global game namespace
 *
 * Created by Andrii on 23.03.2017.
 */
var fl = fl || {};
(function () {
    /**
     * Local game namespace
     */

    fl.startGame = function ()  {
        var renderer = PIXI.autoDetectRenderer(640, 480);
        var gameContainer = document.getElementById('game');
        gameContainer.appendChild(renderer.view);
        var stage = new PIXI.Container();

        renderer.view.style.border = "1px dashed black";
        renderer.backgroundColor = 0x061639;

        var spawnModel = new fl.SpawnModel();
        var spawnView = new fl.SpawnView(spawnModel, stage, renderer);
        var spawnController = new fl.SpawnController(spawnModel, spawnView);

        function gameLoop() {
            requestAnimationFrame(gameLoop);

            spawnView.update();
            spawnController.update();

            renderer.render(stage);
        }

        gameLoop();
    };
})();
window.onload = fl.startGame;